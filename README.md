# Heck Links Bot

This Telegram bot nukes tracking parameters off websites.
Right now it's just the most trivial implementation where it rips off anything after a question mark.
This will break if the site uses query parameters to identify content, but it's good enough for Twitter.

## How to use
Type something like `@HeckLinksBot https://twitter.com/post?tracking=very` into the text box and click on the clean link that appears.

## How to run

### From the registry
1. Create a new bot with the [BotFather](https://t.me/BotFather)
2. Make a copy of [compose.yaml](compose.yaml) on your server and fill in the environment variables
3. Run `docker-compose up -d`

### Build the container yourself
Same steps as _From the registry_, but with the file [compose.build.yaml](compose.build.yaml).

### Environment variables
- `BOT_TOKEN`: The bot token retrieved from the BotFather
