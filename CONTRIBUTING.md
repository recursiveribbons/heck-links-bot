# Contributing

First, thanks for your interest in contributing to the project!

To contribute, fork the project, then create a pull request towards the `main` branch on
this project.

Please remember to never EVER put API keys in your repositories. That's why the configuration for them are all
externalized.

To keep your fork up to date, consider adding this as a remote
with `git remote add upstream git@gitlab.com:recursiveribbons/heck-links-bot.git` and pull periodically, especially before
pushing.

## The replacement list

There is a [yaml file](src/main/resources/application.yml) that contains configurations for exempted hosts and host replacements.
- The `exempted` entry takes a list of exempted hosts - this is where removing the query parameters will break the site.
- The `replacements` entry takes a map of hosts, and what they should be mapped to.

Contributions to this list is welcome.
