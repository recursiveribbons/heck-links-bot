package dev.robinsyl.hecktrack.services;

import dev.robinsyl.hecktrack.config.TrackingConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class TrackingCodeServiceTest {

    TrackingCodeService service;

    TrackingCodeServiceTest() {
        TrackingConfig config = new TrackingConfig(List.of("www.youtube.com"), Map.of("twitter.com", "fxtwitter.com"));
        service = new TrackingCodeService(config);
    }

    @Test
    void testCleanUrl() {
        String result = service.cleanTracking("https://facebook.com/testpost?tracking=123");
        assertThat(result, equalTo("https://facebook.com/testpost"));
    }

    @Test
    void testIgnoredUrl() {
        String result = service.cleanTracking("https://www.youtube.com/watch?v=dQw4w9WgXcQt");
        assertThat(result, equalTo("https://www.youtube.com/watch?v=dQw4w9WgXcQt"));
    }

    @Test
    void testTwitterReplace() {
        String result = service.cleanTracking("https://twitter.com/Someone/status/12345?tracking=123");
        assertThat(result, equalTo("https://fxtwitter.com/Someone/status/12345"));
    }

    @Test
    void testTwitterReplaceTwice() {
        String result = service.cleanTracking("https://fxtwitter.com/Someone/status/12345?tracking=123");
        assertThat(result, equalTo("https://fxtwitter.com/Someone/status/12345"));
    }
}