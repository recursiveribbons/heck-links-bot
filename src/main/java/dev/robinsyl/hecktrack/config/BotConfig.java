package dev.robinsyl.hecktrack.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

@ConfigurationProperties("bot")
public class BotConfig {
    /**
     * Bot token as given by the BotFather
     */
    private final String token;

    @ConstructorBinding
    public BotConfig(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
