package dev.robinsyl.hecktrack.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

import java.util.List;
import java.util.Map;

@ConfigurationProperties("hosts")
public class TrackingConfig {
    /**
     * Comma-separated list of hostnames exempted from query type removal
     */
    private final List<String> exempted;
    /**
     * Map of replacements with the domain to be replaced as the key and the replacement as the value
     */
    private final Map<String, String> replacements;

    @ConstructorBinding
    public TrackingConfig(List<String> exempted, Map<String, String> replacements) {
        this.exempted = exempted;
        this.replacements = replacements;
    }

    public List<String> getExempted() {
        return exempted;
    }

    public Map<String, String> getReplacements() {
        return replacements;
    }
}
