package dev.robinsyl.hecktrack.services;

import dev.robinsyl.hecktrack.config.TrackingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TrackingCodeService {
    private static final Logger logger = Logger.getLogger(TrackingCodeService.class.getName());
    private static final Pattern urlPattern = Pattern.compile("https?://(www\\.)?[-a-zA-Z\\d@:%._+~#=]{1,256}\\.[a-zA-Z\\d()]{1,6}\\b([-a-zA-Z\\d()!@:%_+.~#?&/=]*)");

    private final List<String> exemptedHosts;
    private final Map<String, String> replacements;

    @Autowired
    public TrackingCodeService(TrackingConfig config) {
        this.exemptedHosts = config.getExempted();
        this.replacements = config.getReplacements();
        logger.info(() -> String.format("Found %d exempted hosts", exemptedHosts.size()));
    }

    public String cleanTracking(String string) {
        Matcher matcher = urlPattern.matcher(string);
        return matcher.replaceAll(this::replaceUrl);
    }

    private String replaceUrl(MatchResult matchResult) {
        String rawUrl = matchResult.group();
        URI uri;
        try {
            uri = new URI(rawUrl);
        } catch (URISyntaxException e) {
            return rawUrl;
        }
        String host = uri.getHost();
        if (exemptedHosts.contains(host)) {
            return rawUrl;
        }
        if (replacements.containsKey(host)) {
            rawUrl = rawUrl.replace(host, replacements.get(host));
        }
        return rawUrl.split("\\?")[0];
    }
}
