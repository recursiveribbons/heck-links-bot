package dev.robinsyl.hecktrack;

import dev.robinsyl.hecktrack.config.BotConfig;
import dev.robinsyl.hecktrack.services.TrackingCodeService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;
import org.telegram.telegrambots.longpolling.interfaces.LongPollingUpdateConsumer;
import org.telegram.telegrambots.longpolling.starter.SpringLongPollingBot;
import org.telegram.telegrambots.longpolling.util.LongPollingSingleThreadUpdateConsumer;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.TelegramClient;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

import static java.util.logging.Level.WARNING;

@Component
public class HeckTrackingBot implements SpringLongPollingBot, LongPollingSingleThreadUpdateConsumer {
    private static final Logger logger = Logger.getLogger(HeckTrackingBot.class.getName());

    private final String botToken;
    private final TelegramClient telegramClient;
    private final TrackingCodeService service;
    private final Counter queryCounter;

    public HeckTrackingBot(BotConfig config, TrackingCodeService service, MeterRegistry meterRegistry) {
        this.botToken = config.getToken();
        telegramClient = new OkHttpTelegramClient(botToken);
        this.service = service;
        queryCounter = meterRegistry.counter("bot.command.calls", "command", "query");
    }

    @Override
    public void consume(Update update) {
        try {
            if (update.hasInlineQuery()) {
                queryCounter.increment();
                processInlineQuery(update.getInlineQuery());
            } else if (update.hasMessage()) {
                sendMessage(update.getMessage().getChatId(), "Type something like the following into the Telegram text input box: <code>@HeckLinksBot https://twitter.com/blah?tracking=yes</code>\nThis bot breaks with links where query parameters are essential, but it's good enough for Twitter. Check the source code at <a href=\"https://gitlab.com/recursiveribbons/heck-links-bot\">GitLab</a>");
            }
        } catch (TelegramApiException e) {
            logger.log(WARNING, "Unable to process update", e);
        }
    }

    private void processInlineQuery(InlineQuery inlineQuery) throws TelegramApiException {
        String query = inlineQuery.getQuery();
        List<InlineQueryResult> results;
        if (!query.isBlank()) {
            String result = service.cleanTracking(query);
            String id = UUID.randomUUID().toString().substring(0, 32);
            results = List.of(new InlineQueryResultArticle(id, "Clean link", new InputTextMessageContent(result)));
        } else {
            results = List.of();
        }
        AnswerInlineQuery answerInlineQuery = new AnswerInlineQuery(inlineQuery.getId(), results);
        CompletableFuture<Boolean> future = telegramClient.executeAsync(answerInlineQuery);
        future.exceptionally(t -> {
            logger.log(WARNING, "Unable to process inline query", t);
            return false;
        });
    }

    private void sendMessage(Long id, String message) {
        SendMessage sendMessage = new SendMessage(id.toString(), message);
        sendMessage.setParseMode(ParseMode.HTML);
        try {
            telegramClient.executeAsync(sendMessage);
        } catch (TelegramApiException e) {
            logger.log(WARNING, "Unable to send message", e);
        }
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public LongPollingUpdateConsumer getUpdatesConsumer() {
        return this;
    }
}
